﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LeaderboardItem : MonoBehaviour {
    public string rankString, usernameString, scoreString, facebookID = "";

    public Text rank, username, score;

    public Image profilePic;

    // Use this for initialization
    void Start(){
        rank.text = rankString;
        username.text = usernameString;
        score.text = scoreString;
        if (facebookID != "")
        {
            StartCoroutine(getFBPicture());
        }
    }

    public IEnumerator getFBPicture(){
        var www = new WWW("http://graph.facebook.com/" + facebookID + "/picture?type=square");

        yield return www;

        Texture2D tempPic = new Texture2D(25, 25);

        www.LoadImageIntoTexture(tempPic);
        profilePic.sprite = Sprite.Create(tempPic, new Rect(0, 0, 50, 50), new Vector2(0, 0)); ;

    }
}

﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using Facebook.Unity;

public class GSInviteFriends : MonoBehaviour {
    /*
    public string FriendSelectorTitle = "";
    public string FriendSelectorMessage = "Come play this great game!";
    public string FriendSelectorFilters = "[\"all\",\"app_users\",\"app_non_users\"]";
    public string FriendSelectorData = "{}";
    public string FriendSelectorExcludeIds = "";
    public string FriendSelectorMax = "20";
    */

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /*
    public void InviteFriends() {
        if (FB.IsLoggedIn) {
            new ListInviteFriendsRequest()
            .Send((response) => {
                GSEnumerable<ListInviteFriendsResponse._InvitableFriend> friends = response.Friends;
                GSData scriptData = response.ScriptData;
                Debug.Log(friends);
                Debug.Log(scriptData);
                
                foreach (ListInviteFriendsResponse._InvitableFriend f in response.Friends) {
                    Debug.Log(f.Id);
                    Debug.Log(f.DisplayName);
                    Debug.Log(f.ProfilePic);
                }
            });
        

            
        }
    }*/

    public void InviteFriends() {
        if (FB.IsLoggedIn) {
            FB.AppRequest(
                message: "This game is awesome, join me. now.",
                title: "Invite your friends to join you"
                );
        } else {
            Debug.Log("FB is not logged in.");
        }
    }



    /*
    private void CallAppRequestAsFriendSelector() {
        // If there's a Max Recipients specified, include it
        int? maxRecipients = null;
        if (FriendSelectorMax != "") {
            try {
                maxRecipients = Int32.Parse(FriendSelectorMax);
            } catch (Exception e) {
                Debug.LogError( e.Message);
            }
        }

        // include the exclude ids
        string[] excludeIds = (FriendSelectorExcludeIds == "") ? null : FriendSelectorExcludeIds.Split(',');

        FB.AppRequest(
            FriendSelectorMessage,
            null,
            FriendSelectorFilters,
            excludeIds,
            maxRecipients,
            FriendSelectorData,
            FriendSelectorTitle,
            delegate (IAppRequestResult result) {
                Debug.Log(result.RawResult);
            }

        );
    }*/

}

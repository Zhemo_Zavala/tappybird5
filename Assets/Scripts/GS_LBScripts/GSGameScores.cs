﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GSGameScores : MonoBehaviour {
    public InputField _levelInputField, _scoreInputField;

    public void addNewScore() {
        PostScores(int.Parse(_levelInputField.textComponent.text),
                    int.Parse(_scoreInputField.textComponent.text));
    }

    public void PostScores(int level, int score) {
        new GameSparks.Api.Requests.LogEventRequest_HSPL().Set_LEVEL(level).Set_SCORE(score).Send((response) => {
            if (response.HasErrors) {
                Debug.Log("Failed");
            } else {
                Debug.Log("Succesful");
            }
        });
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using GameSparks.Core;
using GameSparks.Api.Requests;

public class APIManager : MonoBehaviour {
    public Text _userName;
    public Image _profilePic;

    [Header("GameSparks Connection")]
    public Text connectionInfoField;

    // Use this for initialization
    void Awake(){
        
    }
    
    // Use this for initialization
    void Start() {
        GS.GameSparksAvailable += OnGameSparksConnected;
    }

    
    private void OnGameSparksConnected(bool _isConnected) {
        if (_isConnected) {
            Debug.Log("GameSparks Connected...");
        } else {
            Debug.Log("GameSparks Not Connected...");
        }
    }

    /*
    private void SetInit() {
        if (!FB.IsInitialized) {
            //Call FB.Init and once that's complete we'll call 
            Debug.Log("Initializing Facebook...");
            FB.Init(ConnectGameSparksToGameSparks, null);
        }
    }
    */

    public void FacebookLogin(){        
        FB.ActivateApp();
        ConnectGameSparksToGameSparks();
    }

    /// <summary>
    /// We call the FacebookConnect_bttn() method from the scene-UI.
    /// We check to see if the Facebook SDK is initialized, and, if so, then we'll activate the app. If not, then we'll attempt to initialize Facebook.
    /// Once we know the Facebook app is activated, we can attempt to connect the GameSparks user to a Facebook account.
    /// When we are logged-in, we'll then attempt to link the account to your GameSparks user using the Facebook access token.
    /// </summary>
    public void FacebookConnect_bttn() {
        Debug.Log("Connecting Facebook With GameSparks...");// first check if FB is ready, and then login //
        // if its not ready we just init FB and use the login method as the callback for the init method //
        if (!FB.IsInitialized) {
            Debug.Log("Initializing Facebook...");
            FB.Init(ConnectGameSparksToGameSparks, null);
        } else {
            FB.ActivateApp();
            ConnectGameSparksToGameSparks();
        }
    }

    /// <summary>
    /// This method is used as the delegate for FB initialization. 
    /// </summary>
    private void ConnectGameSparksToGameSparks() {
        if (FB.IsInitialized) {
            FB.ActivateApp();
            Debug.Log("Logging Into Facebook...");
            var perms = new List<string>() { "public_profile", "email", "user_friends" };
            FB.LogInWithReadPermissions(perms, (result) => {
                if (FB.IsLoggedIn) {
                    Debug.Log("Logged In, Connecting GS via FB..");
                    new GameSparks.Api.Requests.FacebookConnectRequest()
                        .SetAccessToken(AccessToken.CurrentAccessToken.TokenString)
                        .SetDoNotLinkToCurrentPlayer(false)// we don't want to create a new account so link to the player that is currently logged in
                        .SetSwitchIfPossible(true)//this will switch to the player with this FB account id they already have an account from a separate login
                        .Send((fbauth_response) => {
                            if (!fbauth_response.HasErrors) {
                                FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
                                FB.API("/me/picture?type=square&height=78&width=78", HttpMethod.GET, DisplayProfilePic);
                                //-->connectionInfoField.text = "GameSparks Authenticated With Facebook...";
                                //userNameField.text = fbauth_response.DisplayName;
                            } else {
                                Debug.LogWarning(fbauth_response.Errors.JSON);//if we have errors, print them out
                            }
                        });
                } else {
                    Debug.LogWarning("Facebook Login Failed:" + result.Error);
                }
            });// lastly call another method to login, and when logged in we have a callback
        } else {
            FacebookConnect_bttn();// if we are still not connected, then try to process again
        }
    }

    //GameSparksLogin takes FBResult from FB.Login but we don't use it 
    //for anything
    public void GameSparksLogin(ILoginResult result) {
        //It never hurts to double check it you are logged into Facebook                
        //before trying to log into GameSparks with Facebook
        if (FB.IsLoggedIn) {
            //This is the standard FacebookConnectRequest. This will                        
            //log into GameSparks with your Facebook Profile.
            new FacebookConnectRequest().SetAccessToken(FB.ClientToken).Send((response) =>
            {
                if (response.HasErrors) {
                    Debug.Log("Something failed with connecting with Facebook");
                } else {
                    //Here you can display some information                                                                              //to the user that they are logged in...
                }
            });
        }
    }    

    /// <summary>
    /// Display facebook user profile picture
    /// </summary>
    /// <param name="result"></param>
    void DisplayProfilePic(IGraphResult result) {

        if (result.Texture != null) {
            _profilePic.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 78, 78), new Vector2());
        }

    }

    /// <summary>
    /// Display facebook user name
    /// </summary>
    /// <param name="result"></param>
    void DisplayUsername(IResult result) {
        if (result.Error == null) {
            _userName.text = "" + result.ResultDictionary["first_name"];
        } else {
            Debug.Log(result.Error);
        }
    }

    /// <summary>
    /// It logs out in FB
    /// </summary>
    public void Logout() {
        if (FB.IsLoggedIn) {
            FB.LogOut();
            _profilePic.sprite = Sprite.Create(null, new Rect(0, 0, 78, 78), new Vector2());
            _userName.text = "Username";
            Debug.Log("Logout!");
        } else {
            Debug.Log("There is nobody connected to FB.");
        }
    }

    /*
    public void ShareWithFriends() {
        if (FB.IsLoggedIn) {
            FB.FeedShare(
                string.Empty, //toId
                new System.Uri("http://apps.facebook.com/"+FB.AppId+"/?challenge_brag=guest"), //link
                "PollyCube", //linkName
                "LinkCaption", //linkCaption
                "LinkDescription", //linkDescription
                new System.Uri("https://drive.google.com/file/d/0B47LQmUYzNEKdEhEbU9OVDRRaWM/view?usp=sharing"), //picture
                string.Empty //mediaSource
        );
        }
    }

    public void InviteFriends() {
        FB.AppRequest(
            message: "This game is awesome, join me. now.",
            title: "Invite your friends to join you"
            );
    }*/

}
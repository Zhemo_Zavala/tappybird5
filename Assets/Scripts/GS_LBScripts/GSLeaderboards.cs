﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Api;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;


public class GSLeaderboards : MonoBehaviour{
    [Header("Leaderboards LEVEL")]
    public int m_level = 1;
    public InputField m_levelInput;

    public Transform leaderboardEntryPrefab;
    public Transform leaderboardGrid;
    public int entryCount = 100;

    public List<LeaderboardItem> entries = new List<LeaderboardItem>();

    void Start(){
        m_levelInput.textComponent.text = "1";
    }

    /// <summary>
    /// Get list of score
    /// </summary>
    /// <param name="level"></param>
    public void GetLeaderboard(){

        m_level = int.Parse(m_levelInput.textComponent.text);

        for (int i = 0; i < entries.Count; i++){
            Destroy(entries[i]);
        }

        entries.Clear();
    
        new LeaderboardDataRequest().SetLeaderboardShortCode("HSBL.LEVEL." + m_level).SetEntryCount(entryCount).SetDontErrorOnNotSocial(true).SetSocial(true).Send((response) => {
            clearList();

            foreach (var entry in response.Data){

                LeaderboardItem go = Instantiate(leaderboardEntryPrefab).GetComponent<LeaderboardItem>();
                go.transform.SetParent(leaderboardGrid);

                go.rankString = entry.Rank.ToString();
                go.usernameString = entry.UserName.ToString();
                go.scoreString = entry.GetNumberValue("SCORE").ToString();
                
                //this is the line we add to pull the facebookID from GameSparks.
                go.GetComponent<LeaderboardItem>().facebookID = entry.ExternalIds.GetString("FB");

                entries.Add(go);
            
            }

        });
    }

    /// <summary>
    /// Clear scroll list
    /// </summary>
    public void clearList() {
        foreach (Transform t in leaderboardGrid) {
            GameObject.Destroy(t.gameObject); //Provisional
        }
    }

    /// <summary>
    /// Post Scores
    /// </summary>
    /// <param name="level"></param>
    /// <param name="score"></param>
    public void PostScores(int level, int score) {
        new GameSparks.Api.Requests.LogEventRequest_HSPL().Set_LEVEL(level).Set_SCORE(score).Send((response) => {
            if (response.HasErrors) {
                Debug.Log("Failed");
            } else {
                Debug.Log("Succesful");
            }
        });
    }

}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using Facebook.Unity;

public class PlayFabLeaderboards : MonoBehaviour {

    private static PlayFabLeaderboards _instance;

    public List<FriendInfo> _friends;
    public List<PlayerLeaderboardEntry> _leaderboard;

    [Header("Leaderboards LEVEL")]
    public int m_level = 1;
    public InputField m_levelInput;

    public Transform leaderboardEntryPrefab;
    public Transform leaderboardGrid;
    public int entryCount = 100;

    public List<LeaderboardItemPF> entries = new List<LeaderboardItemPF>();

    // Use this for initialization
    private void Awake() {
        _instance = this;
    }

    /// <summary>
    /// Button to upload leaderboards (for testing)
    /// </summary>
    public void UpdateLeaderBoardsBttn() {
        if(m_levelInput.textComponent.text != null ) GetPFLeaderboard(int.Parse(m_levelInput.textComponent.text));
    }

    #region Leaderboards
    /// <summary>
    /// Get list of score
    /// </summary>
    /// <param name="level"></param>
    public void GetPFLeaderboard(int level) {
        //PlayFabId current user
        string pfID = PlayFabManager._instance.PlayFabId;
        //Statistic name (level)
        string sttsName = PlayFabGameScore.instance.GetStatisticName(level);

        for (int i = 0; i < entries.Count; i++) {
            Destroy(entries[i]);
        }

        entries.Clear();

        //Get Friends
        PlayFabClientAPI.GetFriendsList(new GetFriendsListRequest { IncludeSteamFriends = false,
                                                                    IncludeFacebookFriends = true    
                                                                }, (GetFriendsListResult resFriends) => {
                                                                    _friends = resFriends.Friends;
                                                                }, null);

        //get Leaderboard
        PlayFabClientAPI.GetFriendLeaderboard(new GetFriendLeaderboardRequest { StatisticName = sttsName,
                                                                                            StartPosition = 0,
                                                                                            MaxResultsCount = entryCount,
                                                                                            IncludeSteamFriends = false,
                                                                                            IncludeFacebookFriends = true},
                                               (GetLeaderboardResult  r) => { // on Success
                                                   clearList(); 

                                                   _leaderboard = r.Leaderboard;

                                                   //set prefab properties 
                                                   foreach (PlayerLeaderboardEntry entry in _leaderboard) {

                                                       LeaderboardItemPF go = Instantiate(leaderboardEntryPrefab).GetComponent<LeaderboardItemPF>();
                                                       go.transform.SetParent(leaderboardGrid);

                                                       go._rankString = (entry.Position+1).ToString();
                                                       go._usernameString = entry.DisplayName;
                                                       go._scoreString = entry.StatValue.ToString();

                                                       #region FacebookID

                                                       for (int i=0; i < _friends.Count;i++) {
                                                           if (_friends[i].FriendPlayFabId == entry.PlayFabId) {
                                                               go._facebookID = _friends[i].FacebookInfo.FacebookId;
                                                               Debug.Log("Facebook ID Friend : " + go._facebookID);
                                                           }
                                                           if (_friends[i].FriendPlayFabId == pfID) {
                                                               go._facebookID = _friends[i].FacebookInfo.FacebookId;
                                                               go._usernameString = "Tú";
                                                               Debug.Log("Facebook ID Friend : " + go._facebookID);
                                                           }
                                                       }
                                                       
                                                       #endregion
        
                                                       entries.Add(go);
                                                   }
                                               },
                                               (GetLeaderboardResult) => { // on fail
                                                   OnApiCallError(GetLeaderboardResult);
                                               });
    }

    /// <summary>
    /// Clear scroll list
    /// </summary>
    public void clearList() {
        foreach (Transform t in leaderboardGrid) {
            GameObject.Destroy(t.gameObject); //Provisional
        }
    }
    #endregion

    /// <summary>
    /// On Api Call Error
    /// </summary>
    /// <param name="err"></param>
    void OnApiCallError(PlayFabError err) {
        string http = string.Format("HTTP:{0}", err.HttpCode);
        string message = string.Format("ERROR:{0} -- {1}", err.Error, err.ErrorMessage);
        string details = string.Empty;

        if (err.ErrorDetails != null) {
            foreach (var detail in err.ErrorDetails) {
                details += string.Format("{0} \n", detail.ToString());
            }
        }

        Debug.LogError(string.Format("{0}\n {1}\n {2}\n", http, message, details));
    }
    

}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LeaderboardItemPF : MonoBehaviour {
    public string _rankString, _usernameString, _scoreString, _facebookID = "";

    public Text _rank, _username, _score;

    public Image _profilePic;

    // Use this for initialization
    void Start () {
        _rank.text = _rankString;
        _username.text = _usernameString;
        _score.text = _scoreString;

        if (_facebookID != "") {
            StartCoroutine(getFBPicture());
        }
    }

    /// <summary>
    /// Get FB picture
    /// </summary>
    /// <returns></returns>
    public IEnumerator getFBPicture() {
        var www = new WWW("http://graph.facebook.com/" + _facebookID + "/picture?type=square");

        yield return www;

        Texture2D tempPic = new Texture2D(25, 25);

        www.LoadImageIntoTexture(tempPic);
        _profilePic.sprite = Sprite.Create(tempPic, new Rect(0, 0, 50, 50), new Vector2(0, 0)); 

    }

}

﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events; 
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using Facebook.Unity;

public class PlayFabManager : MonoBehaviour {
    public string PlayFabId;
    [HideInInspector]
    public string titleId;

    [HideInInspector]
    public AccessToken aToken;

    public Text _userName;
    public Image _profilePic;

    public static PlayFabManager _instance;

    [Header("GameSparks Connection")]
    public Text connectionInfoField;

    public UnityEvent GetInventoryEvents = new UnityEvent();
    public UnityEvent PlayEvents = new UnityEvent();

    // Use this for initialization
    void Awake() {
        _instance = this;
        
        //GetInventoryEvents.AddListener(GetComponent<InventoryController>().loadPlayFabData);
        //GetInventoryEvents.AddListener(GetComponent<InventoryController>().loadLeaderBoard);
        //PlayEvents.AddListener(GetComponent<InventoryController>().checkInventory);
    }


    private void InitCallback() {
        if (FB.IsInitialized) {
            // Signal an app activation App Event
            FB.ActivateApp();
            Time.timeScale = 0;
            // Continue with Facebook SDK
            // ...
            var perms = new List<string>() { "public_profile", "email", "user_friends" };
            FB.LogInWithReadPermissions(perms, AuthCallBack);
        } else {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void AuthCallBack(ILoginResult result) {
        if (FB.IsLoggedIn) {
            // AccessToken class will have session details
            aToken = AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions) {
                Debug.Log(perm);
            }

            LoginWithFacebook(aToken.TokenString, true, null);
        } else {
            Debug.Log("User cancelled login");
        }
    }

    public void LoginWithFacebook(string token, bool createAccount = false, UnityAction errCallback = null) {
        //LoginMethodUsed = LoginPathways.facebook;
        LoginWithFacebookRequest request = new LoginWithFacebookRequest();
        request.AccessToken = token;
        request.TitleId = PlayFabSettings.TitleId;
        request.CreateAccount = createAccount;

        PlayFabClientAPI.LoginWithFacebook(request, OnLoginCB, OnApiCallError);
    }

    void OnLoginCB(PlayFab.ClientModels.LoginResult result) {
        Debug.Log(string.Format("Login Successful. Welcome Player:{0}!", result.PlayFabId));
        Debug.Log(string.Format("Your session ticket is: {0}", result.SessionTicket));
        FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
        FB.API("me/picture?type=square&height=78&width=78", HttpMethod.GET, DisplayProfilePic);

        GetInventoryEvents.Invoke();
        PlayEvents.Invoke();

        OnHideUnity(true);

    }

    void OnApiCallError(PlayFabError err) {
        string http = string.Format("HTTP:{0}", err.HttpCode);
        string message = string.Format("ERROR:{0} -- {1}", err.Error, err.ErrorMessage);
        string details = string.Empty;

        if (err.ErrorDetails != null) {
            foreach (var detail in err.ErrorDetails) {
                details += string.Format("{0} \n", detail.ToString());
            }
        }

        Debug.LogError(string.Format("{0}\n {1}\n {2}\n", http, message, details));
    }

    private void OnHideUnity(bool isGameShown) {
        if (!isGameShown) {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        } else {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    // Use this for initialization
    void Start () {
        titleId = PlayFab.PlayFabSettings.TitleId;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Facebook Connect Button
    /// </summary>
    public void FacebookConnect_bttn() {
        if (!FB.IsInitialized) {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        } else {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

        /// <summary>
        /// Display facebook user profile picture
        /// </summary>
        /// <param name="result"></param>
        void DisplayProfilePic(IGraphResult result) {

        if (result.Texture != null) {
            _profilePic.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 78, 78), new Vector2());
        }

    }

    /// <summary>
    /// Display facebook user name
    /// </summary>
    /// <param name="result"></param>
    void DisplayUsername(IResult result) {
        if (result.Error == null) {
            string fbname = result.ResultDictionary["first_name"] as string;
            _userName.text = "" + fbname;
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest { DisplayName = fbname }, null, null);
        } else {
            Debug.Log(result.Error);
        }
    }

    /// <summary>
    /// It logs out in FB
    /// </summary>
    public void Logout() {
        if (FB.IsLoggedIn) {
            FB.LogOut();
            _profilePic.sprite = Sprite.Create(null, new Rect(0, 0, 78, 78), new Vector2());
            _userName.text = "Username";
            Debug.Log("Logout!");
        } else {
            Debug.Log("There is nobody connected to FB.");
        }
    }

    /// <summary>
    /// Invite Friends
    /// </summary>    
    public void InviteFriends() {
        if (FB.IsLoggedIn) {
            FB.AppRequest(
                message: "This game is awesome, join me. now.",
                title: "Invite your friends to join you"
                );
        } else {
            Debug.Log("FB is not logged in.");
        }
    }

}

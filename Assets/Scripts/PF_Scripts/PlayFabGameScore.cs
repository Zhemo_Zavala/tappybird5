﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;

public class PlayFabGameScore : MonoBehaviour {
    //only for testing
    [Header("Level / Score Inputfieds (Only for testing)")]
    public InputField _levelInputField;
    public InputField _scoreInputField;

    public static PlayFabGameScore instance;

    public List<PlayerLeaderboardEntry> leaderboard;

    // Use this for initialization
    private void Awake() {
        instance = this;
    }

    // Use this for initialization
    void Start () {
	
	}

    #region STATISTIC NAME
    /// <summary>
    /// Returns the name of statistic int he format: 'lvl_XXXX'
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    public string GetStatisticName(int level) {
        string stticsName = "";
        if (level < 1) { // < 1
            Debug.LogError("Statistic Name : Invalid Level ( < 1 ).");
            return (stticsName);
        } else if (level > 0 && level < 10) { // 1 - 9
            stticsName = "lvl_000" + level;
        } else if (level > 9 && level < 100 ) { // 10 - 99
            stticsName = "lvl_00" + level;
        } else if (level > 99 && level < 1000) { // 100 - 999
            stticsName = "lvl_0" + level;
        } else if (level > 999 && level < 10000) { // 1000 - 9999
            stticsName = "lvl_" + level;
        }
        return (stticsName);
    }
    #endregion

    /// <summary>
    /// Method to asign to a button for testing
    /// </summary>
    public void SubmitScoreBttn() {
        SubmitScore(int.Parse(_levelInputField.textComponent.text),
                    int.Parse(_scoreInputField.textComponent.text));
    }

    /// <summary>
    /// Submit Score to the PlayFab game
    /// </summary>
    /// <param name="score"></param>
    public void SubmitScore(int level, int score) {
        string statisticName = GetStatisticName(level);

        UpdatePlayerStatisticsRequest req = new UpdatePlayerStatisticsRequest();
        req.Statistics = new List<StatisticUpdate>();
        req.Statistics.Add(new StatisticUpdate { StatisticName = statisticName, Version = 0, Value = score });
        PlayFabClientAPI.UpdatePlayerStatistics(req, null, OnApiCallError);
    }

    /*
    // <summary>
    /// Get high score given a int n level
    /// </summary>
    /// <param name="level"></param>
    /// <returns></returns>
    public int GetLeveHighScore(int level) {
        PlayFabClientAPI.GetCharacterStatistics();
        return 0;
    }*/
    
    /// <summary>
    /// Get Leaderboard from Playfab game
    /// </summary>
    public void GetLeaderboard() {
        PlayFabClientAPI.GetLeaderboard(new GetLeaderboardRequest { StatisticName = "TotalPoints", StartPosition = 0, MaxResultsCount = 10 },
                                                (GetLeaderboardResult r) => {
                                                    leaderboard = r.Leaderboard;
                                                },
                                                null);
    }

    /// <summary>
    /// On Api Call Error
    /// </summary>
    /// <param name="err"></param>
    void OnApiCallError(PlayFabError err) {
        string http = string.Format("HTTP:{0}", err.HttpCode);
        string message = string.Format("ERROR:{0} -- {1}", err.Error, err.ErrorMessage);
        string details = string.Empty;

        if (err.ErrorDetails != null) {
            foreach (var detail in err.ErrorDetails) {
                details += string.Format("{0} \n", detail.ToString());
            }
        }

        Debug.LogError(string.Format("{0}\n {1}\n {2}\n", http, message, details));
    }
}

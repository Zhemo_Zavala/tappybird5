using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
	public class LogEventRequest_HSPL : GSTypedRequest<LogEventRequest_HSPL, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_HSPL() : base("LogEventRequest"){
			request.AddString("eventKey", "HSPL");
		}
		public LogEventRequest_HSPL Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		public LogEventRequest_HSPL Set_LEVEL( long value )
		{
			request.AddNumber("LEVEL", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_HSPL : GSTypedRequest<LogChallengeEventRequest_HSPL, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_HSPL() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "HSPL");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_HSPL SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_HSPL Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		public LogChallengeEventRequest_HSPL Set_LEVEL( long value )
		{
			request.AddNumber("LEVEL", value);
			return this;
		}			
	}
	
}
	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_HSBL : GSTypedRequest<LeaderboardDataRequest_HSBL,LeaderboardDataResponse_HSBL>
	{
		public LeaderboardDataRequest_HSBL() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "HSBL");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_HSBL (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_HSBL SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_HSBL SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_HSBL SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_HSBL SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_HSBL SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_HSBL SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_HSBL SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_HSBL SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_HSBL SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_HSBL : GSTypedRequest<AroundMeLeaderboardRequest_HSBL,AroundMeLeaderboardResponse_HSBL>
	{
		public AroundMeLeaderboardRequest_HSBL() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "HSBL");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_HSBL (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_HSBL SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_HSBL SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_HSBL SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_HSBL SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_HSBL SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_HSBL SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_HSBL SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_HSBL : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_HSBL(GSData data) : base(data){}
		public long? SCORE{
			get{return response.GetNumber("SCORE");}
		}
		public long? LEVEL{
			get{return response.GetNumber("LEVEL");}
		}
	}
	
	public class LeaderboardDataResponse_HSBL : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_HSBL(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_HSBL> Data_HSBL{
			get{return new GSEnumerable<_LeaderboardEntry_HSBL>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_HSBL(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_HSBL> First_HSBL{
			get{return new GSEnumerable<_LeaderboardEntry_HSBL>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_HSBL(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_HSBL> Last_HSBL{
			get{return new GSEnumerable<_LeaderboardEntry_HSBL>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_HSBL(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_HSBL : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_HSBL(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_HSBL> Data_HSBL{
			get{return new GSEnumerable<_LeaderboardEntry_HSBL>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_HSBL(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_HSBL> First_HSBL{
			get{return new GSEnumerable<_LeaderboardEntry_HSBL>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_HSBL(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_HSBL> Last_HSBL{
			get{return new GSEnumerable<_LeaderboardEntry_HSBL>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_HSBL(data);});}
		}
	}
}	

namespace GameSparks.Api.Messages {


}
